function getCommunityID() {
    declare response
    declare -g COMMUNITY_ID
    response=$(curl --silent --url "${baseUrl}/community?name=$1")
    COMMUNITY_ID=$(echo "$response" | jq .community_view.community.id)
}

function createPost() {
    declare -r community="qrf"
    declare -r postTitle="Post created via API 2"
    declare -r postUrl="https://lemmy.cafe"
    declare -r postBody="Something about Lemmy Cafe is just so unbelievably great!"
    declare -r nsfw=false

    getCommunityID "$community"
    [ -v TOKEN ] || getToken

    curl --silent \
	 --url "${baseUrl}/post" \
	 --header 'accept: application/json' \
	 --header 'content-type: application/json' \
	 --data "
{
	\"name\": \"$postTitle\",
	\"community_id\": $COMMUNITY_ID,
	\"url\": \"$postUrl\",
	\"body\": \"$postBody\",
	\"nsfw\": $nsfw,
	\"auth\": $TOKEN
}
" | jq
}
