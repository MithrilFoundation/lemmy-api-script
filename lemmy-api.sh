#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

function startup() {
    umask 027
    WORKDIR=$(mktemp -d)
    declare -l name="${0##*/}"
    name="${name%.sh}" # Avoided using basename!
    LOCKDIR=/tmp
    [ "$EUID" -eq 0 ] && LOCKDIR=/run
    LOCKFILE="${LOCKDIR}/${name}.lock"
    touch "$LOCKFILE"
    echo "$BASHPID" >> "$LOCKFILE"
}

function cleanup() {
    echo "Cleaning things up"
    rm -rf "$WORKDIR"
    declare -F userLogout || source ./lib/auth.sh
    userLogout
}

trap cleanup SIGHUP SIGINT SIGQUIT SIGABRT EXIT

startup

source env.sh
source ./lib/auth.sh

[ "$1" == "create-post" ] && \
    source ./lib/create-post.sh && \
    createPost

[ "$1" == "ban" ] && \
    source ./lib/ban.sh && \
    banPerson "$2"
