function getUsers() {
    declare response
    response=$(curl --silent \
		    --request GET \
		    --url "${baseUrl}/search?q=${1}&type=Users"
	    )
    echo "$response" | jq .users[].person.id
}

function banPerson() {
    declare response
    for asshole in $(getUsers "$1")
    do
	# shellcheck disable=2015 # I want C to run if only A is true
	[ -v TOKEN ] && [ -n "$TOKEN" ] || getToken
	echo "asshole=$asshole"
	response=$(curl --silent \
			--request POST \
			--header 'content-type: application/json' \
			--header "Authorization: Bearer $TOKEN" \
			--url "${baseUrl}/user/ban" \
			--data "
{
	\"ban\": true,
	\"person_id\": $asshole,
	\"remove_data\": true
}
"
		)
	echo "$response" | jq
    done
}
