function getToken() {
    declare -g TOKEN
    declare response
    read -rp "Enter $user TOTP: " totp
    response=$(curl --silent \
		    --url "${baseUrl}/user/login" \
		    --header "Content-Type: application/json" \
		    --request POST \
		    --data "{
		    \"username_or_email\": \"$user\",
		    \"password\": \"$pass\",
		    \"totp_2fa_token\": \"$totp\"
		    }"
	    )
    TOKEN=$(echo "$response" | cut -d',' -f1 | cut -d':' -f2 | tr -d '"')
}

function userLogout() {
    echo "Script finished. Logging out."
    curl --silent \
	 --url "${baseUrl}/user/logout" \
	 --header "Content-Type: application/json" \
 	 --header "Authorization: Bearer $TOKEN" \
	 --request POST | jq
}
